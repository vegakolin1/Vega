import cv2
import numpy as np
from logzero import logger, logfile
from fastiecm import fastiecm
from pathlib import Path
from time import sleep



def display(image, image_name):
    image = np.array(image, dtype=float)/float(255)
    shape = image.shape
    height = int(shape[0] / 2)
    width = int(shape[1] / 2)
    image = cv2.resize(image, (width, height))
    cv2.namedWindow(image_name)
    cv2.imshow(image_name, image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()



def contrast_stretch(im):
    in_min = np.percentile(im, 5)
    in_max = np.percentile(im, 95)
   

    out_min = 0.0
    out_max = 255.0

    out = im - in_min
    out *= ((out_min - out_max) / (in_min - in_max))
    out += in_min

    return out

def calc_ndvi(image):
    b, g, r = cv2.split(image)
    bottom = (r.astype(float) + b.astype(float))
    bottom[bottom==0] = 0.01
    ndvi = (b.astype(float) - r) / bottom
    return ndvi


base_folder = Path(__file__).parent.resolve()
Pocet = 332  
counter = 1
image_file = f"Vega_{counter:03d}.jpg"
contrast = f"VegaContr_{counter:03d}.jpg"
fil_ndvi = f"Vegandvi_{counter:03d}.jpg"
fil_ndvi_contrasted = f"VegandviContr_{counter:03d}.jpg"


while (counter < Pocet):

    try:
        # Načtení obrázku
        # load image
        image_file = f"Vega_{counter:03d}.jpg"
        contrast = f"VegaContr_{counter:03d}.jpg"
        fil_ndvi = f"Vegandvi_{counter:03d}.jpg"
        fil_ndvi_contrasted = f"VegandviContr1_{counter:03d}.jpg"

        # original = cv2.imread(image_file)
        image = cv2.imread(image_file) # load image

        image = np.array(image, dtype=float)/float(255) #convert to an array
        #cv2.namedWindow('Original') # create window
        # cv2.imshow('Original', image) # display image
        #cv2.waitKey(0) # wait for key press
        #cv2.destroyAllWindows()
        # print ('Stop Original')
        print (counter)
       
        # sleep(1)
        # =====================================
        # Tady zobrazí velký originál
        # display(image, 'Original')

        # ===================================== 
        # Tady se zvýší kontrast
        contrasted = contrast_stretch(image)
        
        #display(contrasted, 'Contrasted original')
        #print (contrast)
        # ===================================== 
        # Tady se zapíše zvýšený kontrast malý rozměr
        cv2.imwrite(contrast, contrasted)

        # =====================================
        # Tady se spočítá NDVI
        ndvi = calc_ndvi(contrasted)
        # display(ndvi, 'NDVI')
        ndvi_contrasted = contrast_stretch(ndvi)
        # display(ndvi_contrasted, 'NDVI Contrasted')
        cv2.imwrite(fil_ndvi, ndvi_contrasted)

        # =====================================
        # Tady se mapuje NDVI
        color_mapped_prep = ndvi_contrasted.astype(np.uint8)
        cv2.imwrite('color_mapped_prep.png', color_mapped_prep)
        color_mapped_image = cv2.applyColorMap(color_mapped_prep, fastiecm)
        # display(color_mapped_image, 'Color mapped')
        cv2.imwrite(fil_ndvi_contrasted, color_mapped_image)


        logger.info(f"iteration {counter}")
        counter += 1
        
    except Exception as e:
        logger.error(f'{e.__class__.__name__}: {e}')
        print("An exception occurred")
        #logger.error(f'{e.__class__.__name__}: {e}')