############################################################################
# 
# TEAM VEGA
#
# Základni skola Kolin 
# 
#       K  K     OOO    L     IIIII N     N
#       K K     O   O   L       I   N N   N
#       Kk      O   O   L       I   N  N  N
#       k K     O   O   L       I   N   N N
#       k  K     OOO    LLLLL IIIII N     N   
# 
# 
# 
# 
# MISSION SPACE LAB 2021 - 2022 
# 
############################################################################



from pathlib import Path
from logzero import logger, logfile
from sense_hat import SenseHat
from picamera import PiCamera
from orbit import ISS
from time import sleep
from datetime import datetime, timedelta
from gpiozero import CPUTemperature
from skyfield.api import load


import csv

ephemeris = load('de421.bsp')
timescale = load.timescale()


#   Založení CSV souboru a zápis hlavičky
#   Create a new CSV file and add the header
def create_csv_file(data_file):
    with open(data_file, 'w') as f:
        writer = csv.writer(f)
        header = ("Counter", "Date/time", "Den_Noc","Latitude", "Longitude", "Elevace", "cpu_temp","Teplota", "Vlhkost", "Tlak")
        writer.writerow(header)

#   Otevře CSV souboru a zápis věty
#   Add a row of data to the data_file CSV"""
def add_csv_data(data_file, data):
     with open(data_file, 'a') as f:
        writer = csv.writer(f)
        writer.writerow(data)


"""    
    Convert a `skyfield` Angle to an EXIF-appropriate
    representation (rationals)
    e.g. 98° 34' 58.7 to "98/1,34/1,587/10"

    Return a tuple containing a boolean and the converted angle,
    with the boolean indicating if the angle is negative.
    """
def convert(angle):
    sign, degrees, minutes, seconds = angle.signed_dms()
    exif_angle = f'{degrees:.0f}/1,{minutes:.0f}/1,{seconds*10:.0f}/10'
    return sign < 0, exif_angle

    # Zjištění GPS polohou z EXIF dat
    # Use `camera` to capture an `image` file with lat/long EXIF data.
def capture(camera, image):

    location = ISS.coordinates()

    # Konverze latitude a longitude do EXIF- údajů
    # Convert the latitude and longitude to EXIF-appropriate representations
    south, exif_latitude = convert(location.latitude)
    west, exif_longitude = convert(location.longitude)

    # Nastavení sat z EXIF udajů o aktuální pozici
    # Set the EXIF tags specifying the current location
    camera.exif_tags['GPS.GPSLatitude'] = exif_latitude
    camera.exif_tags['GPS.GPSLatitudeRef'] = "S" if south else "N"
    camera.exif_tags['GPS.GPSLongitude'] = exif_longitude
    camera.exif_tags['GPS.GPSLongitudeRef'] = "W" if west else "E"

    # Zápis obrázku
    # Capture the image
    camera.capture(image)


base_folder = Path(__file__).parent.resolve()

# Set a logfile name
logfile(base_folder/"events.log")

# Set up Sense Hat
sense = SenseHat()

# Set up camera
cam = PiCamera()
cam.resolution = (2592,1944)

# Initialise the CSV file
data_file = base_folder/"data.csv"
create_csv_file(data_file)

# Initialise the photo counter
counter = 1
# Record the start and current time
start_time = datetime.now()

stop_time = start_time + timedelta(minutes=176) # 3 hodiny - 2 minuty
#stop_time = start_time + timedelta(minutes=5)

now_time = datetime.now()
Zbytek_time = stop_time - now_time
# Run a loop for (almost) three hours
while (now_time < stop_time):
    
   
    try:
        t = timescale.now()
        now_time = datetime.now()
        Zbytek_time = stop_time - now_time
        image_file = f"{base_folder}/Vega_{counter:03d}.jpg"
        Fotka = f"Vega_{counter:03d}"
        if ISS.at(t).is_sunlit(ephemeris):
            Den_Noc = "Den"
            capture(cam, image_file)
        else:
            Den_Noc = "Noc"
            Fotka = "Noc"
        # Zaokrouhlení
        # Next Calculate Round
        # 
        humidity = round(sense.humidity, 4)
        temperature = round(sense.temperature, 4)
        pressure = round(sense.pressure, 4)
        cpu = CPUTemperature()
        
        # Get coordinates of location on Earth below the ISS
        location = ISS.coordinates()
        # Save the data to the file
        t = load.timescale().now()
        # print (t)
        # Compute where the ISS is at time `t`
        position = ISS.at(t)
        # Compute the coordinates of the Earth location directly beneath the ISS
        location1 = position.subpoint()
        #--------------------------------------
        
        data = (
            Fotka,
            datetime.now(),
            Den_Noc,
            location.latitude.degrees,
            location.longitude.degrees,
            location.elevation.km,
            cpu.temperature,
            temperature,
            humidity,
            pressure,
            location1,
        )
        add_csv_data(data_file, data)
       
        ###############################################################################################
        logger.info(f"iteration {counter}")
        counter += 1
        # Nastavení nového aktuálního času
        # Update the current time
        now_time = datetime.now()
        #print(f'Now_time na konci ____ {now_time}.')
        sleep(30)
    except Exception as e:
        logger.error(f'{e.__class__.__name__}: {e}')